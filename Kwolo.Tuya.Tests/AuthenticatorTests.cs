﻿using System;
using System.Net;
using System.Net.Http;
using System.Security.Authentication;
using System.Text.Json;
using FakeItEasy;
using Kwolo.Tuya.TuyaConnector;
using Kwolo.Tuya.TuyaConnector.Helpers;
using Kwolo.Tuya.TuyaConnector.Models;
using Kwolo.Tuya.Tests.MockHttpClientHelpers;
using Microsoft.Extensions.Logging;
using Xunit;

namespace Kwolo.Tuya.Tests
{
    public static class AuthenticatorTests
    {
        private static AuthenticatedSession BuildGoodSession()
        {
            return new AuthenticatedSession
            {
                AccessToken = "abc123",
                ExpiresIn = 3600,
                RefreshToken = "xyz987"
            };
        }

        private static HttpResponseMessage BuildHttpResponse(AuthenticatedSession session)
        {
            return new HttpResponseMessage(HttpStatusCode.Accepted)
            {
                Content = new StringContent(JsonSerializer.Serialize(session))
            };
        }

        [Fact]
        public static async void CanGetAuthenticatedSession()
        {
            var httpMessageHandler = new MockHttpMessageHandler();
            var httpClientFactory = new MockHttpClientFactory(httpMessageHandler);

            var auth = new Authenticator(httpClientFactory, new CreateSessionFromResponse(),
                A.Fake<ILogger<Authenticator>>());

            httpMessageHandler.HttpResponseMessage = BuildHttpResponse(BuildGoodSession());

            Assert.NotNull(await auth.AuthenticateAsync("user", "pass", "UK"));
        }

        [Fact]
        public static async void ReturnsAccessToken()
        {
            var httpMessageHandler = new MockHttpMessageHandler();
            var httpClientFactory = new MockHttpClientFactory(httpMessageHandler);

            var auth = new Authenticator(httpClientFactory, new CreateSessionFromResponse(),
                A.Fake<ILogger<Authenticator>>());

            httpMessageHandler.HttpResponseMessage = BuildHttpResponse(BuildGoodSession());

            var session = await auth.AuthenticateAsync("user", "pass", "UK");
            Assert.Equal("abc123", session.AccessToken);
        }

        [Fact]
        public static async void ReturnsRefreshToken()
        {
            var httpMessageHandler = new MockHttpMessageHandler();
            var httpClientFactory = new MockHttpClientFactory(httpMessageHandler);

            var auth = new Authenticator(httpClientFactory, new CreateSessionFromResponse(),
                A.Fake<ILogger<Authenticator>>());

            httpMessageHandler.HttpResponseMessage = BuildHttpResponse(BuildGoodSession());

            var session = await auth.AuthenticateAsync("user", "pass", "UK");
            Assert.Equal("xyz987", session.RefreshToken);
        }

        [Fact]
        public static async void SetsExpiryDate()
        {
            var httpMessageHandler = new MockHttpMessageHandler();
            var httpClientFactory = new MockHttpClientFactory(httpMessageHandler);

            var auth = new Authenticator(httpClientFactory, new CreateSessionFromResponse(),
                A.Fake<ILogger<Authenticator>>());

            httpMessageHandler.HttpResponseMessage = BuildHttpResponse(BuildGoodSession());

            var session = await auth.AuthenticateAsync("user", "pass", "UK");
            var expiresIn = Math.Abs((session.ExpiresAt - DateTime.UtcNow).TotalSeconds);
            Assert.True(expiresIn > 3500 && expiresIn < 3700);
        }

        [Fact]
        public static async void ThrowsOnServerError()
        {
            var httpMessageHandler = new MockHttpMessageHandler();
            var httpClientFactory = new MockHttpClientFactory(httpMessageHandler);

            var auth = new Authenticator(httpClientFactory, new CreateSessionFromResponse(),
                A.Fake<ILogger<Authenticator>>());

            httpMessageHandler.HttpResponseMessage = BuildHttpResponse(BuildGoodSession());
            httpMessageHandler.HttpResponseMessage.StatusCode = HttpStatusCode.InternalServerError;

            var exception = await Record.ExceptionAsync(() => auth.AuthenticateAsync("user", "pass", "UK"));
            Assert.IsType<AuthenticationException>(exception);
            Assert.Contains("message=", exception.Message);
        }
    }
}