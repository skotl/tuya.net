﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using FakeItEasy;
using Kwolo.Tuya.Tests.MockHttpClientHelpers;
using Kwolo.Tuya.TuyaConnector;
using Kwolo.Tuya.TuyaConnector.Exceptions;
using Kwolo.Tuya.TuyaConnector.Helpers;
using Kwolo.Tuya.TuyaConnector.Models;
using Microsoft.Extensions.Logging;
using Xunit;

namespace Kwolo.Tuya.Tests
{
    public static class DeviceSwitcherTests
    {
        private static AuthenticatedSession BuildSession()
        {
            return new AuthenticatedSession
            {
                AccessToken = "EUabc123",
                ExpiresAt = DateTime.Now.AddHours(1),
                ExpiresIn = 3600,
                RefreshToken = "ref111"
            };
        }

        private static HttpResponseMessage BuildHttpResponse(DeviceSwitchResponse response)
        {
            return new HttpResponseMessage(HttpStatusCode.Accepted)
            {
                Content = new StringContent(JsonSerializer.Serialize(response))
            };
        }

        private static ITuyaBaseUrlHelper GetTuyaBaseUrlHelper()
        {
            var helper = A.Fake<ITuyaBaseUrlHelper>();
            A.CallTo(() => helper.GetBaseUrl(""))
                .WithAnyArguments()
                .Returns("https://local.com");

            return helper;
        }

        [Fact]
        public static async void CanSwitchOn()
        {
            var httpMessageHandler = new MockHttpMessageHandler();
            var httpClientFactory = new MockHttpClientFactory(httpMessageHandler);

            httpMessageHandler.HttpResponseMessage = BuildHttpResponse(new DeviceSwitchResponse()
            {
                Header = new Header {Code = "SUCCESS", PayloadVersion = 1}
            });

            var switcher = new DeviceSwitcher(httpClientFactory, GetTuyaBaseUrlHelper(), new TuyaHttpRequestBuilder(),
                A.Fake<ILogger<DeviceSwitcher>>());
            await switcher.SwitchOnAsync("deviceId", BuildSession());
        }

        [Fact]
        public static async void CanSwitchOff()
        {
            var httpMessageHandler = new MockHttpMessageHandler();
            var httpClientFactory = new MockHttpClientFactory(httpMessageHandler);

            httpMessageHandler.HttpResponseMessage = BuildHttpResponse(new DeviceSwitchResponse()
            {
                Header = new Header {Code = "SUCCESS", PayloadVersion = 1}
            });

            var switcher = new DeviceSwitcher(httpClientFactory, GetTuyaBaseUrlHelper(), new TuyaHttpRequestBuilder(),
                A.Fake<ILogger<DeviceSwitcher>>());
            await switcher.SwitchOffAsync("deviceId", BuildSession());
        }

        [Fact]
        public static async void ThrowsOnNullSession()
        {
            var httpMessageHandler = new MockHttpMessageHandler();
            var httpClientFactory = new MockHttpClientFactory(httpMessageHandler);

            var switcher = new DeviceSwitcher(httpClientFactory, GetTuyaBaseUrlHelper(), new TuyaHttpRequestBuilder(),
                A.Fake<ILogger<DeviceSwitcher>>());
            await Assert.ThrowsAsync<ArgumentException>(() => switcher.SwitchOnAsync("deviceId", null));
            await Assert.ThrowsAsync<ArgumentException>(() => switcher.SwitchOffAsync("deviceId", null));
        }

        [Fact]
        public static async void ThrowsOnNullDeviceId()
        {
            var httpMessageHandler = new MockHttpMessageHandler();
            var httpClientFactory = new MockHttpClientFactory(httpMessageHandler);

            var switcher = new DeviceSwitcher(httpClientFactory, GetTuyaBaseUrlHelper(), new TuyaHttpRequestBuilder(),
                A.Fake<ILogger<DeviceSwitcher>>());
            await Assert.ThrowsAsync<ArgumentException>(() => switcher.SwitchOnAsync(null, BuildSession()));
            await Assert.ThrowsAsync<ArgumentException>(() => switcher.SwitchOffAsync(null, BuildSession()));
        }

        [Fact]
        public static async void ThrowsOnEmptyDeviceId()
        {
            var httpMessageHandler = new MockHttpMessageHandler();
            var httpClientFactory = new MockHttpClientFactory(httpMessageHandler);

            var switcher = new DeviceSwitcher(httpClientFactory, GetTuyaBaseUrlHelper(), new TuyaHttpRequestBuilder(),
                A.Fake<ILogger<DeviceSwitcher>>());
            await Assert.ThrowsAsync<ArgumentException>(() => switcher.SwitchOnAsync("   ", BuildSession()));
            await Assert.ThrowsAsync<ArgumentException>(() => switcher.SwitchOffAsync("   ", BuildSession()));
        }

        [Fact]
        public static async void ThrowsOnResponseWithFailure()
        {
            var httpMessageHandler = new MockHttpMessageHandler();
            var httpClientFactory = new MockHttpClientFactory(httpMessageHandler);

            httpMessageHandler.HttpResponseMessage = BuildHttpResponse(new DeviceSwitchResponse()
            {
                Header = new Header {Code = "FAILED", PayloadVersion = 1}
            });

            var switcher = new DeviceSwitcher(httpClientFactory, GetTuyaBaseUrlHelper(), new TuyaHttpRequestBuilder(),
                A.Fake<ILogger<DeviceSwitcher>>());
            await Assert.ThrowsAsync<DeviceSwitchFailedException>(() =>
                switcher.SwitchOnAsync("deviceId", BuildSession()));
        }

        [Fact]
        public static async void ThrowsOnFailedResponse()
        {
            var httpMessageHandler = new MockHttpMessageHandler();
            var httpClientFactory = new MockHttpClientFactory(httpMessageHandler);

            httpMessageHandler.HttpResponseMessage = BuildHttpResponse(null);
            httpMessageHandler.HttpResponseMessage.StatusCode = HttpStatusCode.InternalServerError;

            var switcher = new DeviceSwitcher(httpClientFactory, GetTuyaBaseUrlHelper(), new TuyaHttpRequestBuilder(),
                A.Fake<ILogger<DeviceSwitcher>>());
            await Assert.ThrowsAsync<HttpRequestException>(() => switcher.SwitchOnAsync("deviceId", BuildSession()));
        }
    }
}