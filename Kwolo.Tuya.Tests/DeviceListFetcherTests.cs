﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using FakeItEasy;
using Kwolo.Tuya.Tests.MockHttpClientHelpers;
using Kwolo.Tuya.TuyaConnector;
using Kwolo.Tuya.TuyaConnector.Exceptions;
using Kwolo.Tuya.TuyaConnector.Helpers;
using Kwolo.Tuya.TuyaConnector.Models;
using Microsoft.Extensions.Logging;
using Xunit;

namespace Kwolo.Tuya.Tests
{
    public static class DeviceListFetcherTests
    {
        private static AuthenticatedSession BuildSession()
        {
            return new AuthenticatedSession
            {
                AccessToken = "EUabc123",
                ExpiresAt = DateTime.Now.AddHours(1),
                ExpiresIn = 3600,
                RefreshToken = "ref111"
            };
        }

        private static HttpResponseMessage BuildHttpResponse(DeviceListResponse response)
        {
            return new HttpResponseMessage(HttpStatusCode.Accepted)
            {
                Content = new StringContent(JsonSerializer.Serialize(response))
            };
        }

        private static ITuyaBaseUrlHelper GetTuyaBaseUrlHelper()
        {
            var helper = A.Fake<ITuyaBaseUrlHelper>();
            A.CallTo(() => helper.GetBaseUrl(""))
                .WithAnyArguments()
                .Returns("https://local.com");

            return helper;
        }

        [Fact]
        public static async void CanFetch()
        {
            var httpMessageHandler = new MockHttpMessageHandler();
            var httpClientFactory = new MockHttpClientFactory(httpMessageHandler);

            httpMessageHandler.HttpResponseMessage = BuildHttpResponse(new DeviceListResponse
            {
                Payload = new DeviceListPayload {Devices = new List<Device>()},
                Header = new Header {Code = "SUCCESS", PayloadVersion = 1}
            });

            var fetcher = new DeviceListFetcher(httpClientFactory, GetTuyaBaseUrlHelper(), new TuyaHttpRequestBuilder(),
                A.Fake<ILogger<DeviceListFetcher>>());

            Assert.NotNull(await fetcher.GetDeviceListAsync(BuildSession()));
        }

        [Fact]
        public static async void FetchReturnsDevices()
        {
            var httpMessageHandler = new MockHttpMessageHandler();
            var httpClientFactory = new MockHttpClientFactory(httpMessageHandler);

            var response = new DeviceListResponse
            {
                Payload = new DeviceListPayload {Devices = new List<Device>()},
                Header = new Header {Code = "SUCCESS", PayloadVersion = 1}
            };
            response.Payload.Devices.Add(new Device {Name = "deviceName"});

            httpMessageHandler.HttpResponseMessage = BuildHttpResponse(response);

            var fetcher = new DeviceListFetcher(httpClientFactory, GetTuyaBaseUrlHelper(), new TuyaHttpRequestBuilder(),
                A.Fake<ILogger<DeviceListFetcher>>());
            var deviceList = await fetcher.GetDeviceListAsync(BuildSession());
            Assert.Single(deviceList);
            Assert.Equal("deviceName", deviceList[0].Name);
        }

        [Fact]
        public static async void ThrowsOnNullSession()
        {
            var httpMessageHandler = new MockHttpMessageHandler();
            var httpClientFactory = new MockHttpClientFactory(httpMessageHandler);

            var fetcher =
                new DeviceListFetcher(httpClientFactory, GetTuyaBaseUrlHelper(), new TuyaHttpRequestBuilder(),
                    A.Fake<ILogger<DeviceListFetcher>>());
            await Assert.ThrowsAsync<ArgumentNullException>(() => fetcher.GetDeviceListAsync(null));
        }

        [Fact]
        public static async void ThrowsOnResponseWithFailure()
        {
            var httpMessageHandler = new MockHttpMessageHandler();
            var httpClientFactory = new MockHttpClientFactory(httpMessageHandler);

            httpMessageHandler.HttpResponseMessage = BuildHttpResponse(new DeviceListResponse
            {
                Payload = new DeviceListPayload {Devices = new List<Device>()},
                Header = new Header {Code = "FAILED", PayloadVersion = 1}
            });

            var fetcher =
                new DeviceListFetcher(httpClientFactory, GetTuyaBaseUrlHelper(), new TuyaHttpRequestBuilder(),
                    A.Fake<ILogger<DeviceListFetcher>>());
            await Assert.ThrowsAsync<DeviceListFailedException>(() => fetcher.GetDeviceListAsync(BuildSession()));
        }

        [Fact]
        public static async void ThrowsOnFailedResponse()
        {
            var httpMessageHandler = new MockHttpMessageHandler();
            var httpClientFactory = new MockHttpClientFactory(httpMessageHandler);

            httpMessageHandler.HttpResponseMessage = BuildHttpResponse(null);
            httpMessageHandler.HttpResponseMessage.StatusCode = HttpStatusCode.InternalServerError;

            var fetcher =
                new DeviceListFetcher(httpClientFactory, GetTuyaBaseUrlHelper(), new TuyaHttpRequestBuilder(),
                    A.Fake<ILogger<DeviceListFetcher>>());
            await Assert.ThrowsAsync<HttpRequestException>(() => fetcher.GetDeviceListAsync(BuildSession()));
        }
    }
}