﻿using System;
using System.Net.Http;
using Kwolo.Tuya.TuyaConnector.Helpers;
using Xunit;

namespace Kwolo.Tuya.Tests.HelperTests
{
    public static class TuyaHttpRequestBuilderTests
    {
        private static string ToJson(string s) => s.Replace('\'', '"');

        [Fact]
        public static void ThrowsOnNullUrl()
        {
            Assert.Throws<ArgumentException>(() => new TuyaHttpRequestBuilder().BuildRequest(null, "json"));
        }

        [Fact]
        public static void ThrowsOnEmptyUrl()
        {
            Assert.Throws<ArgumentException>(() => new TuyaHttpRequestBuilder().BuildRequest("   ", "json"));
        }

        [Fact]
        public static void ThrowsOnNullJson()
        {
            Assert.Throws<ArgumentException>(() => new TuyaHttpRequestBuilder().BuildRequest("url", null));
        }

        [Fact]
        public static void ThrowsOnEmptyJson()
        {
            Assert.Throws<ArgumentException>(() => new TuyaHttpRequestBuilder().BuildRequest("url", "   "));
        }

        [Fact]
        public static void CanCreateRequest()
        {
            var req = new TuyaHttpRequestBuilder().BuildRequest("https://local.com", ToJson("{ 'field': 'value' }"));
            
            Assert.Equal("https://local.com/", req.RequestUri.ToString());
        }
        
        [Fact]
        public static void RequestIsPost()
        {
            var req = new TuyaHttpRequestBuilder().BuildRequest("https://local.com", ToJson("{ 'field': 'value' }"));

            Assert.Equal(HttpMethod.Post, req.Method);
        }
        
        [Fact]
        public static void RequestAcceptsJsonResponse()
        {
            var req = new TuyaHttpRequestBuilder().BuildRequest("https://local.com", ToJson("{ 'field': 'value' }"));

            Assert.Equal("application/json", req.Headers.Accept.ToString());
        }
        
        [Fact]
        public static void BodyIsJson()
        {
            var req = new TuyaHttpRequestBuilder().BuildRequest("https://local.com", ToJson("{ 'field': 'value' }"));

            Assert.Equal("application/json; charset=utf-8", req.Content.Headers.ContentType.ToString());
        }
        
        [Fact]
        public static async void BodyContentIsSet()
        {
            var req = new TuyaHttpRequestBuilder().BuildRequest("https://local.com", ToJson("{ 'field': 'value' }"));

            var content = await req.Content.ReadAsStringAsync();
            Assert.Equal(ToJson("{ 'field': 'value' }"), content);
        }
    }
}