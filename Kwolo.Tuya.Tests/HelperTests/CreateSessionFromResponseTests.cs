﻿using System;
using System.Security.Authentication;
using System.Text.Json;
using Kwolo.Tuya.TuyaConnector.Helpers;
using Kwolo.Tuya.TuyaConnector.Models;
using Xunit;

namespace Kwolo.Tuya.Tests.HelperTests
{
    public static class CreateSessionFromResponseTests
    {
        private static string CreateGoodResponse()
        {
            return JsonSerializer.Serialize(new AuthenticatedSession
            {
                AccessToken = "access", ExpiresIn = 60, RefreshToken = "refresh"
            });
        }
        
        [Fact]
        public static void CanCreateSession()
        {
            var session = new CreateSessionFromResponse().GetSession(CreateGoodResponse());
            Assert.NotNull(session);
        }
        
        [Fact]
        public static void SessionHasAccessToken()
        {
            var session = new CreateSessionFromResponse().GetSession(CreateGoodResponse());
            Assert.Equal("access", session.AccessToken);
        }
        
        [Fact]
        public static void SessionHasRefreshToken()
        {
            var session = new CreateSessionFromResponse().GetSession(CreateGoodResponse());
            Assert.Equal("refresh", session.RefreshToken);
        }
        
        [Fact]
        public static void SessionHasExpiresIn()
        {
            var session = new CreateSessionFromResponse().GetSession(CreateGoodResponse());
            Assert.Equal(60, session.ExpiresIn);
        }
        
        [Fact]
        public static void SessionCalculatesExpiry()
        {
            var session = new CreateSessionFromResponse().GetSession(CreateGoodResponse());
            var diff = DateTime.UtcNow.AddSeconds(60) - session.ExpiresAt;
            
            Assert.True(Math.Abs(diff.Seconds) < 5);
        }
        
        [Fact]
        public static void ThrowsOnExpiredSession()
        {
            var session = JsonSerializer.Serialize(new AuthenticatedSession
            {
                AccessToken = "access", ExpiresIn = -10, RefreshToken = "refresh"
            });

            var exception = Record.Exception(() => new CreateSessionFromResponse().GetSession(session));
            Assert.IsType<AuthenticationException>(exception);
            Assert.Contains("expired", exception.Message);
        }

        [Fact]
        public static void ThrowsOnNullSession()
        {
            Assert.Throws<ArgumentException>(() => new CreateSessionFromResponse().GetSession(null));
        }
        
        [Fact]
        public static void ThrowsOnEmptySession()
        {
            Assert.Throws<ArgumentException>(() => new CreateSessionFromResponse().GetSession("  "));
        }

        [Fact]
        public static void ThrowsOnMissingAccessToken()
        {
            var session = JsonSerializer.Serialize(new AuthenticatedSession
            {
                ExpiresIn = 60, RefreshToken = "refresh"
            });

            var exception = Record.Exception(() => new CreateSessionFromResponse().GetSession(session));
            Assert.IsType<AuthenticationException>(exception);
            Assert.Contains("Authentication failed", exception.Message);
        }
    }
}