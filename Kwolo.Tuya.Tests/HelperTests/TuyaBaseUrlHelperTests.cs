﻿using System;
using Kwolo.Tuya.TuyaConnector.Helpers;
using Xunit;

namespace Kwolo.Tuya.Tests.HelperTests
{
    public static class TuyaBaseUrlHelperTests
    {
        [Fact]
        public static void CanGetEuUrl()
        {
            Assert.Equal("https://px1.tuyaeu.com/homeassistant/", new TuyaBaseUrlHelper().GetBaseUrl("EU"));
        }
        
        [Fact]
        public static void CanGetUsUrl()
        {
            Assert.Equal("https://px1.tuyaus.com/homeassistant/", new TuyaBaseUrlHelper().GetBaseUrl("US"));
        }
        
        [Fact]
        public static void CanGetAyUrl()
        {
            Assert.Equal("https://px1.tuyacn.com/homeassistant/", new TuyaBaseUrlHelper().GetBaseUrl("AY"));
        }
        
        [Fact]
        public static void IsCaseInsensitive()
        {
            Assert.Equal("https://px1.tuyaeu.com/homeassistant/", new TuyaBaseUrlHelper().GetBaseUrl("eu"));
            Assert.Equal("https://px1.tuyaus.com/homeassistant/", new TuyaBaseUrlHelper().GetBaseUrl("us"));
            Assert.Equal("https://px1.tuyacn.com/homeassistant/", new TuyaBaseUrlHelper().GetBaseUrl("ay"));
        }

        [Fact]
        public static void ThrowsOnNullAccessToken()
        {
            Assert.Throws<ArgumentException>(() => new TuyaBaseUrlHelper().GetBaseUrl(null));
        }
        
        [Fact]
        public static void ThrowsOnEmptyAccessToken()
        {
            Assert.Throws<ArgumentException>(() => new TuyaBaseUrlHelper().GetBaseUrl("  "));
        }
        
        [Fact]
        public static void ThrowsOnShortAccessToken()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new TuyaBaseUrlHelper().GetBaseUrl("A"));
        }
        
        [Fact]
        public static void ThrowsOnUnknownAccessToken()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new TuyaBaseUrlHelper().GetBaseUrl("????"));
        }
    }
}