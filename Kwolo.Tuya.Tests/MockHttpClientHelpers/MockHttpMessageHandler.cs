﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Kwolo.Tuya.Tests.MockHttpClientHelpers
{
    /// <summary>
    /// An <see cref="HttpMessageHandler"/> that always returns the response stored in <see cref="HttpResponseMessage"/>
    /// </summary>
    public class MockHttpMessageHandler : HttpMessageHandler
    {
        /// <summary>
        /// The <see cref="HttpRequestMessage"/> to return when <see cref="SendAsync"/> is called
        /// </summary>
        public HttpResponseMessage HttpResponseMessage { get; set; }
            
        /// <summary>
        /// Returns a task containing the <see cref="HttpResponseMessage"/> property
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return Task.FromResult(HttpResponseMessage);
        }
    }
}