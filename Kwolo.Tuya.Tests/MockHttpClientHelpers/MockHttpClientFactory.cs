﻿using System.Net.Http;

namespace Kwolo.Tuya.Tests.MockHttpClientHelpers
{
    /// <summary>
    /// An <see cref="IHttpClientFactory"/> that returns a custom <see cref="HttpMessageHandler"/>
    /// </summary>
    internal class MockHttpClientFactory : IHttpClientFactory
    {
        private readonly HttpMessageHandler _httpMessageHandler;

        /// <summary>
        /// An <see cref="IHttpClientFactory"/> that returns a custom <see cref="HttpMessageHandler"/>
        /// </summary>
        /// <param name="httpMessageHandler"></param>
        public MockHttpClientFactory(HttpMessageHandler httpMessageHandler)
        {
            _httpMessageHandler = httpMessageHandler;
        }
        
        /// <summary>
        /// Creates an <see cref="HttpClient"/> that uses the <see cref="HttpMessageHandler"/> passed to the constructor
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public HttpClient CreateClient(string name)
        {
            return new HttpClient(_httpMessageHandler);
        }
    }
}