﻿using System;
using System.Text.Json;
using Kwolo.Tuya.TuyaConnector.Models;
using Xunit;

namespace Kwolo.Tuya.Tests.ModelTests
{
    public static class DeviceSwitchRequestTests
    {
        [Fact]
        public static void ConstructCreatesChildren()
        {
            var req = new DeviceSwitchRequest("abc123", "Device0", 10);
            
            Assert.NotNull(req.Header);
            Assert.NotNull(req.Payload);
        }
        
        [Fact]
        public static void SetsAccessToken()
        {
            var req = new DeviceSwitchRequest("abc123", "Device0", 10);
            
            Assert.Equal("abc123", req.Payload.AccessToken);
        }
        
        [Fact]
        public static void SetsDeviceId()
        {
            var req = new DeviceSwitchRequest("abc123", "Device0", 10);
            
            Assert.Equal("Device0", req.Payload.DeviceId);
        }
        
        [Fact]
        public static void SetsNewState()
        {
            var req = new DeviceSwitchRequest("abc123", "Device0", 10);
            
            Assert.Equal(10, req.Payload.NewState);
        }

        [Fact]
        public static void ThrowsOnNullAccessToken()
        {
            Assert.Throws<ArgumentException>(() => new DeviceSwitchRequest(null, "Device0", 10));
        }
        
        [Fact]
        public static void ThrowsOnEmptyAccessToken()
        {
            Assert.Throws<ArgumentException>(() => new DeviceSwitchRequest("   ", "Device0", 10));
        }
        
        [Fact]
        public static void ThrowsOnNullDeviceId()
        {
            Assert.Throws<ArgumentException>(() => new DeviceSwitchRequest("abc123", null, 10));
        }
        
        [Fact]
        public static void ThrowsOnEmptyDeviceId()
        {
            Assert.Throws<ArgumentException>(() => new DeviceSwitchRequest("abc123", "   ", 10));
        }
        
        [Fact]
        public static void CanSerialise()
        {
            var req = new DeviceSwitchRequest("abc123", "Device0", 10);
            
            using var doc = JsonDocument.Parse(req.AsJson);
            var root = doc.RootElement;
            var payload = root.GetProperty("payload");     // will throw on missing payload
            root.GetProperty("header");                                // will throw on missing header
            
            Assert.Equal("abc123", payload.GetProperty("accessToken").GetString());
        }
    }
}