﻿using Kwolo.Tuya.TuyaConnector.Models;
using Xunit;

namespace Kwolo.Tuya.Tests.ModelTests
{
    public static class DeviceSwitchRequestPayloadTests
    {
        [Fact]
        public static void SetsAccessToken()
        {
            var payload = new DeviceSwitchRequestPayload("abc123", "Device0", 10);
            
            Assert.Equal("abc123", payload.AccessToken);
        }
        
        [Fact]
        public static void SetsDeviceId()
        {
            var payload = new DeviceSwitchRequestPayload("abc123", "Device0", 10);
            
            Assert.Equal("Device0", payload.DeviceId);
        }
        
        [Fact]
        public static void SetsNewState()
        {
            var payload = new DeviceSwitchRequestPayload("abc123", "Device0", 10);
            
            Assert.Equal(10, payload.NewState);
        }
        
    }
}