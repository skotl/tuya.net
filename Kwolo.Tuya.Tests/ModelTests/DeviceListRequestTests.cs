﻿using System.Text.Json;
using Kwolo.Tuya.TuyaConnector.Models;
using Xunit;

namespace Kwolo.Tuya.Tests.ModelTests
{
    public static class DeviceListRequestTests
    {
        [Fact]
        public static void ConstructCreatesChildren()
        {
            var req = new DeviceListRequest("abc123");
            
            Assert.NotNull(req.Header);
            Assert.NotNull(req.Payload);
            Assert.Equal("abc123", req.Payload.AccessToken);
        }

        [Fact]
        public static void CanSerialise()
        {
            var req = new DeviceListRequest("abc123");
            
            using var doc = JsonDocument.Parse(req.AsJson);
            var root = doc.RootElement;
            var payload = root.GetProperty("payload");     // will throw on missing payload
            root.GetProperty("header");                                // will throw on missing header
            
            Assert.Equal("abc123", payload.GetProperty("accessToken").GetString());
        }
    }
}