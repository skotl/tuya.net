﻿using Kwolo.Tuya.TuyaConnector.Models;
using Xunit;

namespace Kwolo.Tuya.Tests.ModelTests
{
    public static class DeviceListRequestHeaderTests
    {
        [Fact]
        public static void SetsName()
        {
            Assert.Equal("Discovery", new DeviceListRequestHeader().Name);
        }
        
        [Fact]
        public static void SetsNamespace()
        {
            Assert.Equal("discovery", new DeviceListRequestHeader().Namespace);
        }
        
        [Fact]
        public static void SetsPayloadVersion()
        {
            Assert.Equal(1, new DeviceListRequestHeader().PayloadVersion);
        }
    }
}