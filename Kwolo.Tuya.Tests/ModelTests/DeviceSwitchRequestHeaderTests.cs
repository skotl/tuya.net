﻿using Kwolo.Tuya.TuyaConnector.Models;
using Xunit;

namespace Kwolo.Tuya.Tests.ModelTests
{
    public static class DeviceSwitchRequestHeaderTests
    {
        [Fact]
        public static void SetsName()
        {
            Assert.Equal("turnOnOff", new DeviceSwitchRequestHeader().Name);
        }
        
        [Fact]
        public static void SetsNamespace()
        {
            Assert.Equal("control", new DeviceSwitchRequestHeader().Namespace);
        }
        
        [Fact]
        public static void SetsPayloadVersion()
        {
            Assert.Equal(1, new DeviceSwitchRequestHeader().PayloadVersion);
        }
    }
}