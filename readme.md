# Tuya.net

## About
.net core library to retrieve Tuya smart home IoT data and switch them on and off

## References
Basic operations pulled from the Javascript library at https://github.com/ndg63276/smartlife with thanks to Github user "Mark W"

## Supported operations
Right now the library supports the following functionality:
1. Authenticate
1. Fetch device list
1. Turn a switch on
1. Turn a switch off

It has only been tested on `bizType=tuya` / `from=tuya` devices, unlike the original Javascript project that also supports `bizType=smart_life` (and possibly others). This wouldn't be hard to configure, but I don't have a way to test them.

## Building
The library is a .net 8 class library, with a helper / sample app as a .net core 8 console app.

## appsettings.json (test harness)
If you want to run the test harness then you'll need to create an `appsettings.json` file in the test harness folder.
The structure of this file should look like this:

```json
{
    "Logging": {
        "LogLevel": {
            "Default": "Debug",
            "System": "Information",
            "Microsoft": "Information",
            "System.Net.Http.HttpClient": "Trace"
        }
    },
    "TestHarnessConfiguration": {
        "UserName": "your tuya login",
        "Password": "your tuya password",
        "CountryCode": "EU"
    }
}
```
The `CountryCode` can be one of:
* EU for Europe / UK
* US for United States / Canada
* AY for China / AsiaPac

## Nuget package
Nuget package is available here: https://www.nuget.org/packages/kwolo.tuya.net

## Sample calls
See the `TestHarness` project for sample code, which contains the following:

```c#
var session = await _authenticatorService.AuthenticateAsync(_config.UserName, _config.Password, _config.CountryCode);

var devices = await _deviceListFetcher.GetDeviceListAsync(session);

await _deviceSwitcher.SwitchOnAsync(devices[0].Id, session);
await Task.Delay(2000);
await _deviceSwitcher.SwitchOffAsync(devices[0].Id, session);
```

First, we authenticate, which returns an `AuthenticatedSession`. That session contains an access code as well as an expiration - be wary of this expiring!

Next, we fetch the list of devices, passing it the authenticated session. 
This is a `List<Device>` - check the `Device` class to see what is returned.

Once we have a list of devices, the test harness assumes that there is at least one, and that it's a switch.
You'll need to do a little sorting through the list of you have multiple devices, and multiple device types.

Finally, we switch the device on via `DeviceSwitcher.SwitchOnAsync()` and then off again, with a two second delay between the operations.

## Contributions welcome!
I actually only wrote this so I could control a single switch, so job done for me. If you have additional Tuya devices and want to contribute to the project then please feel free!

Please note that merge/pull requests without unit tests will probably not get approved...

