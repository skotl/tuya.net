﻿using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Kwolo.Tuya.TuyaConnector.Exceptions;
using Kwolo.Tuya.TuyaConnector.Helpers;
using Kwolo.Tuya.TuyaConnector.Models;
using Microsoft.Extensions.Logging;

namespace Kwolo.Tuya.TuyaConnector
{
    public class DeviceSwitcher : IDeviceSwitcher
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ITuyaBaseUrlHelper _tuyaBaseUrlHelper;
        private readonly ITuyaHttpRequestBuilder _tuyaHttpRequestBuilder;
        private readonly ILogger<DeviceSwitcher> _logger;

        public DeviceSwitcher(IHttpClientFactory httpClientFactory,
            ITuyaBaseUrlHelper tuyaBaseUrlHelper,
            ITuyaHttpRequestBuilder tuyaHttpRequestBuilder,
            ILogger<DeviceSwitcher> logger)
        {
            _httpClientFactory = httpClientFactory;
            _tuyaBaseUrlHelper = tuyaBaseUrlHelper;
            _tuyaHttpRequestBuilder = tuyaHttpRequestBuilder;
            _logger = logger;
        }
        
        public async Task SwitchOnAsync(string deviceId, AuthenticatedSession session)
        {
            await SwitchDevice(deviceId, true, session?.AccessToken);
        }

        public async Task SwitchOffAsync(string deviceId, AuthenticatedSession session)
        {
            await SwitchDevice(deviceId, false, session?.AccessToken);
        }

        private async Task SwitchDevice(string deviceId, bool newState, string accessToken)
        {
            if (string.IsNullOrWhiteSpace(accessToken))
                throw new ArgumentException(nameof(accessToken));
            if (string.IsNullOrWhiteSpace(deviceId))
                throw new ArgumentException(nameof(deviceId));
            
            var baseUrl = _tuyaBaseUrlHelper.GetBaseUrl(accessToken) + "skill";

            var data = new DeviceSwitchRequest(accessToken, deviceId, newState ? 1 : 0).AsJson;
            var request = _tuyaHttpRequestBuilder.BuildRequest(baseUrl, data);         
            
            var response = await _httpClientFactory.CreateClient().SendAsync(request);
            
            if (!response.IsSuccessStatusCode)
                LogAndThrow(new HttpRequestException($"Status={response.StatusCode}, message={response.ReasonPhrase}"));

            var responseText = await response.Content.ReadAsStringAsync();
            var responseStructure = JsonSerializer.Deserialize<DeviceSwitchResponse>(responseText);
            
            if (responseStructure.Header?.Code != "SUCCESS")
                throw new DeviceSwitchFailedException(responseStructure.Header?.Code);
        }
        
        private void LogAndThrow(Exception e)
        {
            _logger.LogError(e, e.Message);
            throw e;
        }
    }
}