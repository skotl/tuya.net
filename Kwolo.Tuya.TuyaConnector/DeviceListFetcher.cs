﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Kwolo.Tuya.TuyaConnector.Exceptions;
using Kwolo.Tuya.TuyaConnector.Helpers;
using Kwolo.Tuya.TuyaConnector.Models;
using Microsoft.Extensions.Logging;

namespace Kwolo.Tuya.TuyaConnector
{
    public class DeviceListFetcher : IDeviceListFetcher
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ITuyaBaseUrlHelper _tuyaBaseUrlHelper;
        private readonly ITuyaHttpRequestBuilder _tuyaHttpRequestBuilder;
        private readonly ILogger<DeviceListFetcher> _logger;

        public DeviceListFetcher(IHttpClientFactory httpClientFactory,
            ITuyaBaseUrlHelper tuyaBaseUrlHelper,
            ITuyaHttpRequestBuilder tuyaHttpRequestBuilder,
            ILogger<DeviceListFetcher> logger)
        {
            _httpClientFactory = httpClientFactory;
            _tuyaBaseUrlHelper = tuyaBaseUrlHelper;
            _tuyaHttpRequestBuilder = tuyaHttpRequestBuilder;
            _logger = logger;
        }

        public async Task<List<Device>> GetDeviceListAsync(AuthenticatedSession session)
        {
            if (session == null)
                throw new ArgumentNullException(nameof(session));
            
            var baseUrl = _tuyaBaseUrlHelper.GetBaseUrl(session.AccessToken) + "skill";

            var data = new DeviceListRequest(session.AccessToken).AsJson;
            var request = _tuyaHttpRequestBuilder.BuildRequest(baseUrl, data);           
            
            var response = await _httpClientFactory.CreateClient().SendAsync(request);
            
            if (!response.IsSuccessStatusCode)
                LogAndThrow(new HttpRequestException($"Status={response.StatusCode}, message={response.ReasonPhrase}"));

            var responseText = await response.Content.ReadAsStringAsync();
            var responseStructure = JsonSerializer.Deserialize<DeviceListResponse>(responseText);
            
            if (responseStructure.Header?.Code != "SUCCESS")
                throw new DeviceListFailedException(responseStructure.Header?.Code);

            return responseStructure.Payload.Devices;
        }

        private void LogAndThrow(Exception e)
        {
            _logger.LogError(e, e.Message);
            throw e;
        }
    }
}