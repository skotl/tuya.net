﻿using System.Threading.Tasks;
using Kwolo.Tuya.TuyaConnector.Models;

namespace Kwolo.Tuya.TuyaConnector
{
    public interface IAuthenticator
    {
        Task<AuthenticatedSession> AuthenticateAsync(string userName, string password, string region);
    }
}