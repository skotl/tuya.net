﻿using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;

namespace Kwolo.Tuya.TuyaConnector.Models
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    public class DeviceListResponse
    {
        [JsonPropertyName("payload")]
        public DeviceListPayload Payload { get; set; }
        
        [JsonPropertyName("header")]
        public Header Header { get; set; }
    }
}