﻿using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;

namespace Kwolo.Tuya.TuyaConnector.Models
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    public class DeviceSwitchRequestPayload
    {
        [JsonPropertyName("accessToken")]
        public string AccessToken { get; }

        [JsonPropertyName("devId")]
        public string DeviceId { get; }

        [JsonPropertyName("value")]
        public int NewState { get; }
            
        public DeviceSwitchRequestPayload(string accessToken, string deviceId, int newState)
        {
            AccessToken = accessToken;
            DeviceId = deviceId;
            NewState = newState;
        }
    }
}