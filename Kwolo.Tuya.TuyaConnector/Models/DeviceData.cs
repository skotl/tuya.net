﻿using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;

namespace Kwolo.Tuya.TuyaConnector.Models
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public class DeviceData
    {
        [JsonPropertyName("online")]
        public bool Online { get; set; }
        
        [JsonPropertyName("state")]
        public bool State { get; set; }
    }
}