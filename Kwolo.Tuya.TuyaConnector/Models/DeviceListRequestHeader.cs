﻿using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;

namespace Kwolo.Tuya.TuyaConnector.Models
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public class DeviceListRequestHeader
    {
        [JsonPropertyName("name")]
        public string Name { get; } = "Discovery";
            
        [JsonPropertyName("namespace")]
        public string Namespace { get; } = "discovery";
            
        [JsonPropertyName("payloadVersion")]
        public int PayloadVersion { get; } = 1;
    }
}