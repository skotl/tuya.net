﻿using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;

namespace Kwolo.Tuya.TuyaConnector.Models
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public class Device
    {
        [JsonPropertyName("data")]
        public DeviceData Data { get; set; }
        
        [JsonPropertyName("name")]
        public string Name { get; set; }
        
        [JsonPropertyName("icon")]
        public string Icon { get; set; }
        
        [JsonPropertyName("id")]
        public string Id { get; set; }
        
        [JsonPropertyName("dev_type")]
        public string DevType { get; set; }
        
        [JsonPropertyName("ha_type")]
        public string HaType { get; set; }

        public override string ToString()
        {
            return $"{Name} ({DevType})";
        }
    }
}