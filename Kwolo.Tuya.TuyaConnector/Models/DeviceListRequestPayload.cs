﻿using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;

namespace Kwolo.Tuya.TuyaConnector.Models
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    public class DeviceListRequestPayload
    {
        [JsonPropertyName("accessToken")]
        public string AccessToken { get; set; }
    }
}