﻿using System.Diagnostics.CodeAnalysis;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Kwolo.Tuya.TuyaConnector.Models
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    public class DeviceListRequest
    {
        [JsonPropertyName("header")]
        public DeviceListRequestHeader Header { get; }

        [JsonPropertyName("payload")]
        public DeviceListRequestPayload Payload { get; }

        [JsonIgnore]
        public string AsJson => JsonSerializer.Serialize(this);
        
        public DeviceListRequest(string accessToken)
        {
            Header = new DeviceListRequestHeader();
            Payload = new DeviceListRequestPayload {AccessToken = accessToken};
        }
    }
}