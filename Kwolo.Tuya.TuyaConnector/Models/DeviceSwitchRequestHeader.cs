﻿using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;

namespace Kwolo.Tuya.TuyaConnector.Models
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    public class DeviceSwitchRequestHeader
    {
        [JsonPropertyName("name")]
        public string Name { get; } = "turnOnOff";
            
        [JsonPropertyName("namespace")]
        public string Namespace { get; } = "control";
            
        [JsonPropertyName("payloadVersion")]
        public int PayloadVersion { get; } = 1;
    }
}