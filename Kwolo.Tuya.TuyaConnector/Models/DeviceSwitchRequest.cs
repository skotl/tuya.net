﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Kwolo.Tuya.TuyaConnector.Models
{
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    public class DeviceSwitchRequest
    {
        [JsonPropertyName("header")] public DeviceSwitchRequestHeader Header { get; }

        [JsonPropertyName("payload")] public DeviceSwitchRequestPayload Payload { get; }

        [JsonIgnore] public string AsJson => JsonSerializer.Serialize(this);

        public DeviceSwitchRequest(string accessToken, string deviceId, int newState)
        {
            if (string.IsNullOrWhiteSpace(accessToken))
                throw new ArgumentException(nameof(accessToken));
            if (string.IsNullOrWhiteSpace(deviceId))
                throw new ArgumentException(nameof(deviceId));

            Header = new DeviceSwitchRequestHeader();
            Payload = new DeviceSwitchRequestPayload(accessToken, deviceId, newState);
        }
    }
}