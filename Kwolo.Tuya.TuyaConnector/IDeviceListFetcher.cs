﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Kwolo.Tuya.TuyaConnector.Models;

namespace Kwolo.Tuya.TuyaConnector
{
    public interface IDeviceListFetcher
    {
        Task<List<Device>> GetDeviceListAsync(AuthenticatedSession session);
    }
}