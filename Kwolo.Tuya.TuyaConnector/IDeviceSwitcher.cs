﻿using System.Threading.Tasks;
using Kwolo.Tuya.TuyaConnector.Models;

namespace Kwolo.Tuya.TuyaConnector
{
    public interface IDeviceSwitcher
    {
        Task SwitchOnAsync(string deviceId, AuthenticatedSession session);
        Task SwitchOffAsync(string deviceId, AuthenticatedSession session);
    }
}