﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Authentication;
using System.Threading.Tasks;
using Kwolo.Tuya.TuyaConnector.Helpers;
using Kwolo.Tuya.TuyaConnector.Models;
using Microsoft.Extensions.Logging;

namespace Kwolo.Tuya.TuyaConnector
{
    public class Authenticator : IAuthenticator
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ICreateSessionFromResponse _createSessionFromResponse;
        private readonly ILogger<Authenticator> _logger;
        private const string BaseUrl = "https://px1.tuyaeu.com/homeassistant/auth.do";

        public Authenticator(IHttpClientFactory httpClientFactory, 
            ICreateSessionFromResponse createSessionFromResponse,
            ILogger<Authenticator> logger)
        {
            _httpClientFactory = httpClientFactory;
            _createSessionFromResponse = createSessionFromResponse;
            _logger = logger;
        }

        public async Task<AuthenticatedSession> AuthenticateAsync(string userName, string password, string region)
        {
            var webClient = _httpClientFactory.CreateClient();
            var data = BuildContent(userName, password, region);

            var response = await webClient.PostAsync(new Uri(BaseUrl), data);
            if (!response.IsSuccessStatusCode)
                LogAndThrow(new AuthenticationException($"Status={response.StatusCode}, message={response.ReasonPhrase}"));

            _logger.LogDebug($"Authentication request returned {response}");
            
            return _createSessionFromResponse.GetSession(await response.Content.ReadAsStringAsync());
        }


        private static HttpContent BuildContent(string userName, string password, string region)
        {
            var postStruct = new[]
            {
                new KeyValuePair<string, string>("userName", userName),
                new KeyValuePair<string, string>("password", password),
                new KeyValuePair<string, string>("countryCode", region),
                new KeyValuePair<string, string>("bizType", "tuya"),
                new KeyValuePair<string, string>("from", "tuya")
            };

            return new FormUrlEncodedContent(postStruct);
        }

        private void LogAndThrow(Exception e)
        {
            _logger.LogError(e, e.Message);
            throw e;
        }
    }
}