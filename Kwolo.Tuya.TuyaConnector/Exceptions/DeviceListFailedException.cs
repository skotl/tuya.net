﻿using System;

namespace Kwolo.Tuya.TuyaConnector.Exceptions
{
    public class DeviceListFailedException : Exception
    {
        public DeviceListFailedException(string msg)
            : base(msg)
        {}
    }
}