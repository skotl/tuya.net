﻿using System;

namespace Kwolo.Tuya.TuyaConnector.Exceptions
{
    public class DeviceSwitchFailedException : Exception
    {
        public DeviceSwitchFailedException(string msg)
            : base(msg)
        {}
    }
}