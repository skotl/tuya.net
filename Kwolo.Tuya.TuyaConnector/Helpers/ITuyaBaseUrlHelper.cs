﻿namespace Kwolo.Tuya.TuyaConnector.Helpers
{
    public interface ITuyaBaseUrlHelper
    {
        string GetBaseUrl(string accessToken);
    }
}