﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace Kwolo.Tuya.TuyaConnector.Helpers
{
    public class TuyaHttpRequestBuilder : ITuyaHttpRequestBuilder
    {
        public HttpRequestMessage BuildRequest(string url, string json)
        {
            if (string.IsNullOrWhiteSpace(url))
                throw new ArgumentException(nameof(url));
            if (string.IsNullOrWhiteSpace(json))
                throw new ArgumentException(nameof(json));

            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(url),
                Method = HttpMethod.Post,
                Content = new StringContent(json, Encoding.UTF8, "application/json")
            };

            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return request;
        }
    }
}