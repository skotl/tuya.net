﻿using System.Net.Http;

namespace Kwolo.Tuya.TuyaConnector.Helpers
{
    public interface ITuyaHttpRequestBuilder
    {
        HttpRequestMessage BuildRequest(string url, string json);
    }
}