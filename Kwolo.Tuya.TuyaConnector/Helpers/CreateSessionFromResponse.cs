using System;
using System.Security.Authentication;
using System.Text.Json;
using Kwolo.Tuya.TuyaConnector.Models;

namespace Kwolo.Tuya.TuyaConnector.Helpers
{
    public class CreateSessionFromResponse : ICreateSessionFromResponse
    {
        public AuthenticatedSession GetSession(string response)
        {
            if (string.IsNullOrWhiteSpace(response))
                throw new ArgumentException(nameof(response));
            
            var session = JsonSerializer.Deserialize<AuthenticatedSession>(response);
            if (string.IsNullOrWhiteSpace(session?.AccessToken))
                throw new AuthenticationException("Authentication failed");

            session.ExpiresAt = DateTime.UtcNow.AddSeconds(session.ExpiresIn);
            
            if (session.ExpiresAt < DateTime.UtcNow)
                throw new AuthenticationException("Session has expired");
            
            return session;
        }
    }
}
