﻿using Kwolo.Tuya.TuyaConnector.Models;

namespace Kwolo.Tuya.TuyaConnector.Helpers
{
    public interface ICreateSessionFromResponse
    {
        AuthenticatedSession GetSession(string response);
    }
}