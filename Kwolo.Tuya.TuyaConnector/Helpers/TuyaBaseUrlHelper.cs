﻿using System;

namespace Kwolo.Tuya.TuyaConnector.Helpers
{
    public class TuyaBaseUrlHelper : ITuyaBaseUrlHelper
    {
        private const string BaseUrl = "https://px1.tuyaeu.com/homeassistant/";

        public string GetBaseUrl(string accessToken)
        {
            if (string.IsNullOrWhiteSpace(accessToken))
                throw new ArgumentException(nameof(accessToken));

            accessToken = accessToken.Trim().ToUpper();
            if (accessToken.Length < 2)
                throw new ArgumentOutOfRangeException(nameof(accessToken));

            return accessToken?.Substring(0, 2) switch
            {
                "AY" => BaseUrl.Replace("eu", "cn"),
                "EU" => BaseUrl,
                "US" => BaseUrl.Replace("eu", "us"),
                _ => throw new ArgumentOutOfRangeException(
                    $"Don't know how to process token beginning '{accessToken.Substring(0, 2)}'")
            };
        }
    }
}