﻿using System.Threading.Tasks;

namespace Kwolo.Tuya.TestHarness
{
    public interface IDoWork
    {
        Task RunAsync();
    }
}