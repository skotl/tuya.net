﻿using Kwolo.Tuya.TuyaConnector;
using Kwolo.Tuya.TuyaConnector.Helpers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Kwolo.Tuya.TestHarness
{
    public class Startup
    {
        private IConfigurationRoot Configuration { get; }

        public Startup()
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(Configuration)
                .AddLogging(loggingBuilder =>
                {
                    loggingBuilder.AddConfiguration(Configuration.GetSection("logging"))
                        .AddConsole();
                })
                .Configure<TestHarnessConfiguration>(Configuration.GetSection("TestHarnessConfiguration"))
                .AddHttpClient()
                .AddScoped<IAuthenticator, Authenticator>()
                .AddScoped<IDoWork, DoWork>()
                .AddScoped<IDeviceListFetcher, DeviceListFetcher>()
                .AddScoped<IDeviceSwitcher, DeviceSwitcher>()
                .AddScoped<ICreateSessionFromResponse, CreateSessionFromResponse>()
                .AddScoped<ITuyaBaseUrlHelper, TuyaBaseUrlHelper>()
                .AddScoped<ITuyaHttpRequestBuilder, TuyaHttpRequestBuilder>()
                .AddScoped<IDeviceListFetcher, DeviceListFetcher>();
        }
    }
}