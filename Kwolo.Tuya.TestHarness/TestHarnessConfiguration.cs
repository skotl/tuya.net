﻿using System.Diagnostics.CodeAnalysis;

namespace Kwolo.Tuya.TestHarness
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
    public class TestHarnessConfiguration
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string CountryCode { get; set; }
    }
}