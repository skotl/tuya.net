﻿using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace Kwolo.Tuya.TestHarness
{
    // ReSharper disable once ClassNeverInstantiated.Global
    // ReSharper disable once ArrangeTypeModifiers
    class Program
    {
        // ReSharper disable once ArrangeTypeMemberModifiers
        static async Task Main(string[] args)
        {
            var services = new ServiceCollection();
            var startup = new Startup();
            startup.ConfigureServices(services);
            
            var serviceProvider = services.BuildServiceProvider();

            var worker = serviceProvider.GetService<IDoWork>();
            await worker.RunAsync();
        }
    }
}