﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kwolo.Tuya.TuyaConnector;
using Kwolo.Tuya.TuyaConnector.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Kwolo.Tuya.TestHarness
{
    public class DoWork : IDoWork
    {
        private readonly IAuthenticator _authenticatorService;
        private readonly IDeviceListFetcher _deviceListFetcher;
        private readonly IDeviceSwitcher _deviceSwitcher;
        private readonly ILogger<DoWork> _logger;
        private readonly TestHarnessConfiguration _config;

        public DoWork(IAuthenticator authenticatorService,
            IDeviceListFetcher deviceListFetcher,
            IDeviceSwitcher deviceSwitcher,
            ILogger<DoWork> logger,
            IOptions<TestHarnessConfiguration> config)
        {
            _authenticatorService = authenticatorService;
            _deviceListFetcher = deviceListFetcher;
            _deviceSwitcher = deviceSwitcher;
            _logger = logger;
            _config = config.Value;
        }

        public async Task RunAsync()
        {
            try
            {
                _logger.LogInformation("Starting...");
                
                var session = await _authenticatorService.AuthenticateAsync(_config.UserName, _config.Password, _config.CountryCode);

                var devices = await _deviceListFetcher.GetDeviceListAsync(session);
                DumpDevices(devices);

                foreach (var device in devices.Where(d => d.DevType == "switch" && d.Data.Online))
                {
                    _logger.LogInformation("Switching {Device}", device);

                    await _deviceSwitcher.SwitchOnAsync(device.Id, session);
                    await Task.Delay(2000);
                    await _deviceSwitcher.SwitchOffAsync(device.Id, session);
                }

                _logger.LogInformation("Shutting down...");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        private void DumpDevices(List<Device> devices)
        {
            devices.ForEach(d => _logger.LogInformation("Found: {Device}", d));
        }
    }
}